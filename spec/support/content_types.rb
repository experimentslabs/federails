ACTIVITYPUB_CONTENT_TYPES = [
  'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
  'application/activity+json',
  'application/json',
].freeze
